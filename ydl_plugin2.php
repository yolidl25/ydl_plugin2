<?php 

/*
Plugin Name: ydl_plugin2
Plugin URI: yolidl25.esy.es
Description: Metadatos de video para mis posts
Version: 1.0
Author: Noir
License: Nada
*/

add_action('add_meta_boxes', 'ydl_add_metabox');
add_action('save_post', 'ydl_save_metabox');
add_action('widgets_init', 'ydl_widget_init');

function ydl_add_metabox() {
	//doc http://codex.wordpress.org/Funcion_Reference/add_meta_box
	add_meta_box('ydl_youtube', 'Youtube Video Link', 'ydl_youtube_handler', 'post');
}

function ydl_youtube_handler() {
    $value = get_post_custom($post->ID);
    $youtube_link = esc_attr($value['ydl_youtube'][0]);
    $youtube_link2 = esc_attr($value['ydl_youtube2'][0]);
    echo '<label for="ydl_youtube">YouTube Video Link</label><input type="text" id="ydl_youtube" name="ydl_youtube" value="'.$youtube_link.'" />';
    echo '<label for="ydl_youtube2">Titulo Video Link</label><input type="text" id="ydl_youtube2" name="ydl_youtube2" value="'.$youtube_link2.'">';
}

/**
 * save metadata
 */
function ydl_save_metabox($post_id) {
    //don't save metadata if it's autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return; 
    }    
    
    //check if user can edit post
    if( !current_user_can( 'edit_post' ) ) {
        return;  
    }
    
    if( isset($_POST['ydl_youtube'] )) {
        update_post_meta($post_id, 'ydl_youtube', $_POST['ydl_youtube']);
    }
}

function ydl_widget_init() {
	register_widget(Ydl_Widget);
}

//Tenemos que crear la clase YDL_Widget
class ydl_Widget extends WP_Widget {
    function ydl_Widget() {
        $widget_options = array(
            'classname' => 'ydl_class', //CSS
            'description' => 'Show a YouTube Video from post metadata'
        );
        
        $this->WP_Widget('ydl_id', 'YouTube Video', $widget_options);
    }
    
    /**
     * show widget form in Appearence / Widgets
     */
    function form($instance) {
        $defaults = array('title' => 'Video');
        $instance = wp_parse_args( (array) $instance, $defaults);
        
        $title = esc_attr($instance['title']);
        
        echo '<p>Title <input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'" /></p>';
    }
    
    /**
     * save widget form
     */
    function update($new_instance, $old_instance) {
        
        $instance = $old_instance;        
        $instance['title'] = strip_tags($new_instance['title']);        
        return $instance;
    }
    
    /**
     * show widget in post / page
     */
    function widget($args, $instance) {
        extract( $args );        
        $title = apply_filters('widget_title', $instance['title']);
        
        //show only if single post
        if(is_single()) {
            echo $before_widget;
            echo $before_title.$title.$after_title;
            
            //get post metadata
            $ydl_youtube = get_post_meta(get_the_ID(), 'ydl_youtube', true);
            $ydl_youtube2 = get_post_meta(get_the_ID(), 'ydl_youtube2', true);
            
            //print widget content
            echo '<iframe width="200" height="200" frameborder="0" allowfullscreen src="http://www.youtube.com/embed/'.($ydl_youtube).'"></iframe>';
            echo $ydl_youtube2;
            
            echo $after_widget;
        }
    }
}

 ?>